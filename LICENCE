DESCRIPTION
	1. TEMPLATE
	The template, in original template form, is licensed under GNU AGPLv3.
	In case of conflict between GNU AGPLv3 and this LICENCE, this LICENCE
	shall have precedence over GNU AGPLv3.
	A single file is excepted, refer to the LICENCE section of this file.

	While the template is in template form it may not be used for any
	project. Every single newly added, modified or otherwise manipulated
	file that has any form of interaction or relationship with the template,
	in template form, shall be licensed under this LICENCE.

	Modification of the template, including the init.sh program, or the
	operating system environment that result in sources still existing
	post-initialisation that would have been deleted by the init.sh program
	without such modifications is prohibited.

	2a. USAGE IN FREE PROJECTS
	When initialising the template for use with a free project the licence
	of all post-initialisation files shall be in the free licence of your
	choice. You may of course change the licence to another free licence in
	the future, any number of times, when you want to do so.

	2b. USAGE IN NON-FREE PROJECTS
	For non-free, also known as proprietary, projects this template may only
	be used with explicit permission of all copyright holders.
	Such permission is typically granted after some form of payment.
	The licence of all post-initialisation files is to be determined on a
	case-by-case basis depending on the demands made by the requester.

	A special exception exists for key contributors, refer to CONTRIBUTORS.

	3. ATTRIBUTION
	Attributing or mentioning this template in any way is not required, but
	would be appreciated by the template creators.

	4. FURTHER USAGE RESTRICTIONS
	None.

	5. WARRANTY
	None.

GLOSSARY
	FREE PROJECT is a project that fully respects the four freedoms, for all
	sources, of any kind, and all artefacts produced, of any kind.

	In a typical project that would mean the source code, documentation,
	build system and binary artefacts have to respect the four freedoms,
	among all other things that are related and/or relevant.


	FOUR FREEDOMS are published by the Free Software Foundation and GNU.
	A copy of the four freedoms can be found below for your convenience.

	FREEDOM 0
		The freedom to run the program as you wish, for any purpose.
	FREEDOM 1
		The freedom to study how the program works, and change it so it
		does your computing as you wish. Access to the source code is a
		precondition for this.
	FREEDOM 2
		The freedom to redistribute copies so you can help others.
	FREEDOM 3
		The freedom to distribute copies of your modified versions to
		others. By doing this you can give the whole community a chance
		to benefit from your changes. Access to the source code is a
		precondition for this.

	Source: https://www.gnu.org/philosophy/free-sw.html.


	NON-FREE PROJECT is a project that does not fully respect the four
	freedoms. If even one of the sources, of any kind, and/or one of the
	artefacts produced, of any kind, are not compliant with the four
	freedoms the entire project is to be considered non-free.


	POST-INITIALISATION FILES are the files that remain after program
	init.sh completes execution successfully in a pristine, which is equal
	to a freshly cloned, repository of the template.

	Proof of successful completion shall be the removal of this LICENCE file
	by the init.sh program as it finishes its execution.


	SOURCES are files and/or contents of files, whether in text form, binary
	form or any other form, that are part of any kind of project. Sources
	are defined as the data itself, thus moving sources between files with
	different licences shall not change the licence of the moved sources.


	TEMPLATE FORM is the template in its original form, prior to execution
	of the init.sh program, still containing this LICENCE file among other
	original sources that the init.sh program manipulates.

LICENCE DETAILS
	This template, in template form, is licensed under the GNU AGPLv3. In
	case of conflict between GNU AGPLv3 and this LICENCE, this LICENCE shall
	have precedence over GNU AGPLv3.

	Refer to file LICENCE.AGPLv3 for details.
	SHA256: 0d96a4ff68ad6d4b6f1f30f713b18d5184912ba8dd389f86aa7710db079abcb0
	Source: https://www.gnu.org/licenses/agpl-3.0.txt

	Refer to the DESCRIPTION the licence basics, this section only provides
	additional information and further references. Usage restrictions, among
	others things, are only documented in the DESCRIPTION section.

WARRANTY
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
	IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
	OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
	ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.

	Note: This section is borrowed from the warranty section of the
	Unlicense licence, which in turn borrowed it from the MIT/X11 licence.

COPYRIGHT HOLDERS
	The vermware business entity holds full copyright over this template.
	Dutch chamber of commerce number: 75921065.

INQUIRIES
	Contact Melvin Vermeeren <vermeeren@vermwa.re> via email.

CONTRIBUTORS
	Melvin Vermeeren <mail@mel.vin>
	Stefan Schrijvers <ximin@ximinity.net>
	Rowan Goemans <goemansrowan@gmail.com>
	Renken <renken@verm.im>

	The above legal entities have contributed greatly to this template in
	the past. Acknowledging their work, they may use this template for
	non-free projects without any form of payment, provided that they hold
	full copyright over each and every of the projects in question.

	The licence of all post-initialisation files in such a case may be any
	licence desired, including non-free licences. They may of course change
	the licence to another licence, of any kind, in the future, any number
	of times, when they want to do so.

METADATA
	vermware template "copyleft-or-pay" licence
	Version 1.1, 2020-06-17.
