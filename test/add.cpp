#include <c_template/add.h>
#include <doctest/doctest.h>

constexpr uint8_t A = 3U;
constexpr uint8_t B = 5U;

SCENARIO("add two numbers")
{
	GIVEN("two numbers")
	{
		uint8_t a = A;
		REQUIRE(a == A);

		uint8_t b = B;
		REQUIRE(b == B);

		WHEN("they are added together")
		{
			uint16_t add1 = add(a, b);
			uint16_t add2 = add(b, a);

			THEN("the result is correct")
			{
				REQUIRE(add1 == a + b);
				REQUIRE(add2 == a + b);
			}
		}
	}
}
