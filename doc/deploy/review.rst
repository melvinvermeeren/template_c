Per-branch documentation review
===============================

.. note::

	You need a webserver with ``rsync`` (secure) or some other way to
	upload and delete things from a CI job like ``ftp`` (insecure).

For every commit pushed to any branch generated documentation is published to a
webserver in the style of ``some.tld/${GROUP}/${PROJECT}``. For the template
itself ``rsync`` is used over SSH as it is most secure.

The advantage of having a deployment per branch is that the reviewer of a merge
request doesn't have to check out locally and build documentation in order to
look at the changes. A link to the online environment is shown on the merge
request page itself.

The deployment environment is dynamically created when a new branch is created
on GitLab. The environment is also dynamically stopped and cleaned up after the
branch has been deleted, usually when the merge request has been accepted.

The setup used by the template has roughly the following steps:

* On the target host, create a user e.g., ``doc`` with a home directory e.g.,
  ``/var/www/doc.mel.vin``, optionally a system user. Access to a valid shell
  ``/bin/sh`` is *required*.

* Change the home directory's permissions to ``750`` owned by the previously
  created user and the group ``www-data``.

* Change ``.ssh`` permissions to ``700``.

* Edit ``.ssh/authorized_keys`` to have the following line
  ``restrict,command="/usr/bin/perl /usr/share/doc/rsync/scripts/rrsync -wo
  subdir" ssh-ed25519 ...`` where ``subdir`` is your project or your group's
  directory e.g., ``template/c``.

* Set the following variables in the CICD variables tab under settings
  on GitLab.

	* ``REVIEW_SSH_KEY``
	* ``REVIEW_SSH_KNOWNHOSTS``
	* ``REVIEW_SSH_DEST`` e.g., ``doc@doc.mel.vin:c``
	* ``REVIEW_URL`` e.g., ``https://doc.mel.vin/template/c``

GitLab has more documentation regarding this:

* https://about.gitlab.com/features/review-apps/
* https://docs.gitlab.com/ee/ci/review_apps/
