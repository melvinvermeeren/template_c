#!/bin/sh

set -eu

hash m4

help()
{
	printf '%s\n' "usage: $0 command
command:
	project: initialize a project with configuration conf.sh.
	template: update the template with configuration init/default.sh.
	help: print this help.

If no command or an unrecognised command is given this help will be printed."
}

[ "$#" -eq 0 ] && help && exit 1
command="$1"

# default values
if hash git; then
	author="$(git config --get user.name || true)"
	mail="$(git config --get user.email || true)"
fi
arch=
author="${author:-$(id -un)}"
copyright=
description=__DESCRIPTION__
mail="${mail:-__AUTHOR_MAIL__}"
project="$(basename "$PWD")"
registry_url='$CI_REGISTRY_IMAGE/cicd'
use_gitlab_ci=
version=0.0.0

case "$command" in
template)
	. init/default.sh
	;;
project)
	. ./conf.sh
	;;
help)
	help
	exit 0
	;;
*)
	help
	exit 1
	;;
esac

# default value of copyright depends on the value of author
[ ! "$copyright" ] && copyright="$(date +'%Y'), $author"

# assert that arch is set
[ "$use_gitlab_ci" ] && [ "$arch" ]

if [ "$command" != template ]; then
	printf 'Project name: %s\n' "$project"
	printf 'Project author: %s\n' "$author"
	printf 'Project email: %s\n' "$mail"
	printf 'Project version: %s\n' "$version"
	printf 'Copyright: %s\n' "$copyright"
	printf 'Description: %s\n' "$description"
	printf 'Use GitLab CI? %s\n' "${use_gitlab_ci:-no}"
	[ "$use_gitlab_ci" ] && printf 'Registry URL: %s\n' "$registry_url"
	[ "$use_gitlab_ci" ] && \
		printf 'GitLab runner architecture: %s\n' "$arch"
	printf 'Apply selected values [Y/n]? '

	read -r answer
	[ "$answer" ] && [ "$answer" != 'y' ] && [ "$answer" != 'Y' ] && exit 0
fi

# cicd setup
if [ "$use_gitlab_ci" ]; then
	<<-EOF m4 -EP \
		-D "__ARCH__=$arch" \
		-D "__INIT_COMMAND__=$command" \
		-D "__NAME__=$project" \
		-D "__REG_URL__=$registry_url" \
		-- - init/gitlab-ci.yml \
		>.gitlab-ci.yml
		m4_changequote(\`[[[',\`]]]')m4_dnl
	EOF
else
	rm -- .gitlab-ci.yml \
		.gitmodules
	rm -r -- cicd \
		doc/cicd \
		doc/deploy \
		doc/docker \
		doc/package
fi

# cmake setup
<<-EOF m4 -EP \
	-D "__AUTHOR_MAIL__=$mail" \
	-D "__AUTHOR__=$author" \
	-D "__COPYRIGHT__=$copyright" \
	-D "__DESCRIPTION__=$description" \
	-D "__PROJECT__=$project" \
	-D "__VERSION__=$version" \
	-- - init/CMakeLists.txt \
	>CMakeLists.txt
	m4_changequote(\`[[[',\`]]]')m4_dnl
EOF

# update harded-coded values
if [ "$project" != c_template ]; then
	for i in src/*.c test/*.cpp include/c_template/*.h; do
		prj_upper="$(printf '%s\n' "$project" \
			| tr '[:lower:]' '[:upper:]')"
		<"$i" sed -e "s/c_template/$project/g" \
			-e "s/C_TEMPLATE/$prj_upper/g" >"$i.tmp"
		mv -- "$i.tmp" "$i"
	done
	mv -- include/c_template "include/$project"

	for i in cmake/pkg/c_template.*; do
		mv -- "$i" "$(printf '%s\n' "$i" \
			| sed "s|c_template|$project|g")"
	done
fi

[ "$command" = template ] && exit 0

# code owners
rm -- CODEOWNERS
if [ "$mail" ] && [ "$mail" != __AUTHOR_MAIL__ ]; then
	printf '* %s\n' "$mail" >CODEOWNERS
fi

# post-init cleanup
rm -rf -- .git
rm -r -- doc/fuzz \
	init
rm -- init.sh \
	conf.sh \
	README.md \
	CHANGELOG.md \
	LICENCE.AGPLv3 \
	LICENCE
